package main

import (
	"time"
	"log"
	"net/http"
	"github.com/veloc1/bitbucket-commenter/web"
)

/*
Что нужно сделать:
сделать сервер, который слушает хуки
задать правила для репозиториев
в заисимости от репозитория нужно сделать коммент используя правила
*/
func main() {
	var webHandler http.Handler = &web.WebHandler{}

	s := &http.Server{
		Addr:           ":8080",
		Handler:        webHandler,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	log.Fatal(s.ListenAndServe())
}
