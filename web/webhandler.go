package web

import (
	"net/http"
	"encoding/json"
)

type WebHandler struct {
	http.Handler
}

type BitbucketLink struct {
	Href string
}

type BitbucketLinks struct {
	Comments BitbucketLink
}

type BitbucketPR struct {
	Links BitbucketLinks
}

type BitbucketEvent struct {
	Pullrequest BitbucketPR
}

type Response struct {
	Url string `json:"url"`
}

func (handler *WebHandler) ServeHTTP(w http.ResponseWriter, r *http.Request ) {
	if r.Method == http.MethodPost {
		var decoder = json.NewDecoder(r.Body)
		var event BitbucketEvent
		err := decoder.Decode(&event)
		if err != nil {
			panic(err)
		}
		w.WriteHeader(http.StatusOK)

		json.NewEncoder(w).Encode(Response{Url: event.Pullrequest.Links.Comments.Href})
	} else {
		w.WriteHeader(http.StatusBadRequest)
		w.Header().Set("Content-Type", "application/json; charset=utf-8") 
	}
}