package test

import (
	"bytes"
	"net/http"
	"encoding/json"
    "net/http/httptest"
	"testing"
	"github.com/veloc1/bitbucket-commenter/web"
)

func TestEmptyHook(t *testing.T) {
	req, err := http.NewRequest("GET", "/", nil)
	if err != nil {
        t.Fatal(err)
	}
	
	rr := httptest.NewRecorder()
	handler := http.Handler(&web.WebHandler{})
 
	handler.ServeHTTP(rr, req)
 
	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusBadRequest)
	}
}

func TestCorrectBitbucketJson(t *testing.T) {
	body := web.BitbucketEvent{}
	body.Pullrequest = web.BitbucketPR{}
	body.Pullrequest.Links = web.BitbucketLinks{}
	body.Pullrequest.Links.Comments = web.BitbucketLink{}
	body.Pullrequest.Links.Comments.Href = "https://url"

	b := new(bytes.Buffer)
	json.NewEncoder(b).Encode(body)

	req, err := http.NewRequest("POST", "/", b)
	if err != nil {
        t.Fatal(err)
	}
	
	rr := httptest.NewRecorder()
	handler := http.Handler(&web.WebHandler{})
 
	handler.ServeHTTP(rr, req)
 
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
 
	expected := `https://url`
	
	var resp web.Response
	var decoder = json.NewDecoder(rr.Body)
	err = decoder.Decode(&resp)
	if err != nil {
		t.Error(err)
	}

	if resp.Url != expected {
		t.Errorf("handler returned unexpected body url: got %v want %v",
			resp.Url, expected)
	}
}